import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'



import App from './App.vue'
import Home from './pages/home.vue'
import Tehnology from './pages/tehnology.vue'
import Partners from './pages/partners.vue'
import Projects from './pages/projects.vue'
import About from './pages/about.vue'

import Grid from 'flexboxgrid-sass'


Vue.use(VueRouter)
Vue.use(VueResource)

const routes = [
  { path: '/', component: Home },  
  { path: '/tehnology', component: Tehnology },
  { path: '/partners', component: Partners },
  { path: '/projects', component: Projects },
  //{ path: '/project/:id', component: project },
  { path: '/about', component: About },  
]

export const router = new VueRouter({
	mode: 'history',
	routes
})


new Vue({
	router,
	el: '#app',
	render: h => h(App)
})